class PublicationsController < ApplicationController
  before_action :authenticate_user!, only: :show
  before_action :check_for_subscription, only: :show

  def index
    @publications = Publication.order "created_at DESC"
  end

  def show
    @publication = Publication.find params[:id]
  end

  def check_for_subscription
    unless current_user.subscription.active
      redirect_to publications_path, alert: "You must be subscribed."
    end
  end
end